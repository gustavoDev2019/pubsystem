
import swal from 'sweetalert2'

export function logoutService(data) {
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Sua sessão expirou, por favor logue novamente',
    })
    return true
}