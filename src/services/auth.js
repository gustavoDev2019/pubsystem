import axios from 'axios'

export function login(credentials) {
  let headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8'
  }
  //let host = "http://standardpub.com.br/api/auth/login"
// let host = "http://127.0.0.1:8000/api/auth/login"
let host = process.env.VUE_APP_URL + '/api/auth/login'
 // let host = '/api/auth/login'
  return new Promise((res, rej) => {
    axios.post(host, credentials, headers)
      .then(response => {
        res(response.data);
      })
      .catch(err => {
        console.log(err);
        rej("Wrong email or password");
      })
  })
}

export function logout(token) { 
  let headers = {
    'Authorization': 'Bearer ' + token, 
    'Content-Type': 'application/json',
  };
  //let host = "http://standardpub.com.br/api/auth/logout"
  //let host = "http://127.0.0.1:8000/api/auth/logout"
  //let host = process.env.VUE_APP_URL + '/api/auth/logout'
  //let host = '/api/auth/logout'
  return new Promise((res, rej) => {
     axios({
          method: 'get',
          url: host,
          headers
        })
        .then((response) => {
           res(response.data);
        })
        .catch((err) => {
           console.log(err);
        })
  })
}

export function getLocalUser() {
  const userString = localStorage.getItem("user");

  if(!userString){
    return null;
  }

  return JSON.parse(userString)
}