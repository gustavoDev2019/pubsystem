import axios from 'axios';

export function apiService(url, method, data, token) {
  let host = process.env.VUE_APP_URL
  if(host) url = host + url

  let headers = {
    'Authorization': 'Bearer ' + token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json;charset=UTF-8',
  }

  return new Promise((res, rej) => {
    axios({
      method,
      url,
      headers,
      data
    }).then(response => {
      res(response.data)
    }).catch(err => {
      rej("")
    })
  })
}

export function fileUpload(url, method, data, token) {
  let host = process.env.VUE_APP_URL
  if(host) url = host + url

  let headers = {
    'Authorization': 'Bearer ' + token,
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'multipart/form-data',
  }

  return new Promise((res, rej) => {
    axios({
      method,
      url,
      headers,
      data
    }).then(response => {
      res(response.data)
    }).catch(err => {
      rej("")
    })
  })
}