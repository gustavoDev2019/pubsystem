import { mapGetters } from 'vuex';
import axios from 'axios'

export function initialize(store, router){
   router.beforeEach((to, from, next) => {
      /* verify if the page we want to go needs to be authenticated */
      const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
      //const currentUser = store.state.currentUser;

      const currentUser = store.getters['login/currentUser'];

      if(requiresAuth && !currentUser) {
         next('/login');
      }else if(to.path == '/login' && currentUser) {
         next('/dashboard');
      }else {
         next();
      }
   })

 /*
 * In case of expired Token from JWT, the user will 
 * be logged out and redirected to the login page
 */
   axios.interceptors.response.use(null, (error) => {
      if (error.response.status == 401) {
         store.commit('login/logout');
         router.push('/login');
      }

      return Promise.reject(error);
   }); 

   /*Create a default header for all axios call
   axios.defaults.headers.common['Authorization'] = `Bearer ${store.getters.currentUser ? store.getters.currentUser.token: ''}`;
   */
   
}