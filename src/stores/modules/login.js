import { getLocalUser } from "@/services/auth";
import { apiService } from "@/services/apiServices";

const user = getLocalUser();

export default {
   namespaced: true,
   state: {
      currentUser: user,
      isLoggedIn: !!user,
      loading: false,
      auth_error: null,
      generalInfo: [],
      generalInfoError: null,
   },
   getters: {
      isLoading(state) {
         return state.loading;
      },
      isLoggedIn(state) {
         return state.isLoggedIn;
      },
      currentUser(state) {
         return state.currentUser;
      },
      authError(state) {
         return state.auth_error;
      },
      generalInfo(state){
         return state.generalInfo;
      },
      generalInfoError(state){
         return state.generalInfoError
      }
   },
   /* Handlers change in the state, is trigger when a 
   * commit is called
   */
   mutations: {
      login(state) {
         state.loading = true;
         state.auth_error = null;
      },
      loginSuccess(state, payload) {
        state.auth_error = null;
        state.isLoggedIn = true;
        state.loading = false;
        state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

        /*verificar retorno da api ao logar, quais dados serão mantido no front*/
        // state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});
         localStorage.setItem("user", JSON.stringify(state.currentUser));
      },
      loginFailed(state, payload){
         state.loading = false;
         state.auth_error = payload.error;
      },
      logout(state) {
         localStorage.removeItem("user");
         state.isLoggedIn = false;
         state.currentUser = null;
      },
      updateGeneralInfo(state, payload) {
         state.generalInfo = payload;
      },
      generalInfoError(state, payload) {
         state.generalInfoError = payload;
      }
   },
   /* Actions/Methods of the store, called by a dispatch,
   * used when want to trigger an action that is asyncronic
   */
   actions: {
      login(context) {
         context.commit("login");
      },
      getGeneralInfo(context) {
         apiService('/api/general-info', 'get', [], context.rootState.login.currentUser.token)
            .then((res) => {
               context.commit('updateGeneralInfo', res);
            })
            .catch((error) => {
               context.commit("generalInfoError", {error});
            });
      }      
   }
}