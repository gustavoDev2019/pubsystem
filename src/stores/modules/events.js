import { apiService, fileUpload } from "@/services/apiServices"
import { logoutService } from "@/services/error-service"
import swal from 'sweetalert2'

export default {
  namespaced: true,
  state: {
    loading: false,
    eventList: [],
    eventListSize: 0,
    event: [],
    promotions: [],
  },
  getters: {
    loading(state) {
      return state.loading
    },
    eventList(state) {
      return state.eventList
    },
    eventListSize(state) {
      return state.eventListSize
    },
    event(state) {
      return state.event
    },
    promotions(state) {
      return state.promotions
    }
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    },
    updateEventList(state, payload) {
      state.eventList = payload
    },
    updateEventListSize(state, payload) {
      state.eventListSize = payload
    },
    updateEvent(state, payload) {
      state.event = payload
    },
    updatePromotions(state, payload) {
      state.promotions = payload
    },
    deleteEvent(state, payload) {
      for(let i = 0; i < state.eventList.length; i++){
        if(state.eventList[i].id == payload.id){
          state.eventList.splice(i, 1)    
        }
      }
    }
  },
  actions: {
    eventList(context) {
      context.commit('setLoading', true)
      apiService("/api/events", 'get', [], context.rootState.login.currentUser.token)
        .then((res) => {
          context.commit('updateEventList', res.data)
          context.commit('setLoading', false)
        })
        .catch((error) => {
          if(logoutService(error)){
            context.commit("login/logout")
            this.$router.push({path: '/login'})
          }
        })
    },
    getEvent(context, data) {
      apiService("/api/events/" + data.id, 'get', [], context.rootState.login.currentUser.token)
        .then(res => {
          context.commit('updateEvent', res.data.event)
          context.commit('updatePromotions', res.data.promotions)
        })
        .catch((error) => {
          if(logoutService(error)){
            context.commit("login/logout")
            this.$router.push({path: '/login'})
          }
        })
    },
    newEvent(context, data) {
      let photo_bg = data.photo_bg
      let photo_card = data.photo_card
      let formData = new FormData()

      context.commit('setLoading', true)

      apiService('/api/events', 'post', data, context.rootState.login.currentUser.token)
        .then((res) => {

          if(res.status == 'error'){
            context.commit('setLoading', false)
            swal.fire({
              title: 'Erro"',
              text: res.data,
              type: 'error',
              confirmButtonText: 'Fechar'
            })
            return
          }

          formData.append('file_bg', photo_bg)
          formData.append('file_card', photo_card)
          formData.append('id', res.data.id)
          fileUpload('/api/upload', 'post', formData, context.rootState.login.currentUser.token)
            .then(res => {
              if(res.status == 'ok'){
                swal.fire({
                  title: 'Ok!',
                  text: 'Evento Cadastrado com sucesso!',
                  type: 'success',
                  confirmButtonText: 'Fechar'
                })
              }else{
                 swal({
                    text: res.data,
                    icon: "error",
                    button: "ok",
                  });
              }
              context.commit('setLoading', false)
            })
        })
        .catch((error) => {
          context.commit('setLoading', false)
          if(logoutService(error)){
            context.commit("login/logout")
            this.$router.push({path: '/login'})
          }
        })
    },
    editEvent(context, data) {
      apiService('/api/events/' + data.id, 'put', data, context.rootState.login.currentUser.token)
        .then(res => {

          if(res.status == 'error'){
            context.commit('setLoading', false)
            swal.fire({
              title: 'Erro"',
              text: res.data,
              type: 'error',
              confirmButtonText: 'Fechar'
            })
            return
          }
          context.commit('updateEvent', res.data.event)

          if(!data.new_photo_card && !data.new_photo_bg){
            swal.fire({
              title: 'Ok!',
              text: 'Evento Atualizado com sucesso!',
              type: 'success',
              confirmButtonText: 'Fechar'
            })
            return;
          }

          let formData = new FormData()
          formData.append('file_bg', data.photo_bg)
          formData.append('file_card', data.photo_card)
          formData.append('id', data.id)
          fileUpload('/api/updateFiles', 'post', formData, context.rootState.login.currentUser.token)
            .then(res => {
              if(res.status == 'ok'){
                swal.fire({
                  title: 'Ok!',
                  text: 'Evento Atualizado com sucesso!',
                  type: 'success',
                  confirmButtonText: 'Fechar'
                })
              }else{
                 swal({
                    text: res.data,
                    icon: "error",
                    button: "ok",
                  });
              }
              context.commit('setLoading', false)
            })
        })
        .catch(error => {
          if(logoutService(error)){
            context.commit("login/logout")
            this.$router.push({path: '/login'})
          }
        })
    },
    deleteEvent(context, data) {
      apiService('/api/events/' + data.id, 'delete', [], context.rootState.login.currentUser.token)
        .then(res => {
          if(res.status == 'ok'){
            let payload = { id: data.id}
            context.commit('deleteEvent', payload)
            swal.fire(
              'Evento excluído!',
              'Operação realizada com sucesso!',
              'success'
            )
          }
        })
        .catch(error => {
          console.log(error)
          if(logoutService(error)){
            context.commit("login/logout")
            this.$router.push({path: '/login'})
          }
        })
    }
  }
}