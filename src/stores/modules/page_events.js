import { apiService, fileUpload } from "@/services/apiServices"

export default {
  namespaced: true,
  state: {
    loading: false,
    eventList: [],
    monthsFilter: [],
    nextEvents: [],
    event: [],
    promotions: [],
  },
  getters: {
    loading(state) {
      return state.loading
    },
    eventList(state) {
      return state.eventList
    },
    monthsFilter(state) {
      return state.monthsFilter
    },
    event(state){
      return state.event
    },
    promotions(state) {
      return state.promotions
    },
    nextEvents(state) {
      return state.nextEvents
    }
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    },
    updateEventList(state, payload) {
      state.eventList = payload
    },
    updateMonthsFilter(state, payload) {
      state.monthsFilter = payload
    },
    updateEvent(state, payload) {
      state.event = payload
    },
    updatePromotions(state, payload){
      state.promotions = payload
    },
    updateNextEvents(state, payload){
      state.nextEvents = payload
    }
  },
  actions: {
    eventList(context) {
      apiService("/api/public/event", 'get', [], null)
        .then((res) => {
          res.data.forEach((event, index) => {
            let img = event.photo_bg.split('/')[2]
            res.data[index].photo_bg = `/storage/public/${img}`
            img = event.photo_card.split('/')[2]
            res.data[index].photo_card = `/storage/public/${img}`
          })
          context.commit('updateEventList', res.data)
          
          let months = []
          res.months.forEach(event => {
            months.push(event.month);
          })
          context.commit('updateMonthsFilter', months)

        })
        .catch((error) => {
          console.log(error)
          //context.commit('')
        })
    },
    getEvent(context, data) {
      apiService("/api/public/event/" + data.id, 'get', [], null)
        .then(res => {
          let img = res.data.event.photo_bg.split('/')[2]
          res.data.event.photo_bg = `/storage/public/${img}`
          
          context.commit('updateEvent', res.data.event)
          context.commit('updatePromotions', res.data.promotions)
        })
        .catch((error) => {
          console.log(error)
          //context.commit('')
        })
    },
    nextEvents(context) {
      apiService("/api/public/next-events", 'get')
        .then(res => {
          res.data.forEach((event, index) => {
            let img = event.photo_card.split('/')[2]
            res.data[index].photo_card = `/storage/public/${img}`
          })
          context.commit('updateNextEvents', res.data)
        })
        .catch(error => {
          console.log(error)
        })
    }
  }
}