import Vue from 'vue';
import Vuex from 'vuex';
import login from './modules/login';
import events from './modules/events';
import page_events from './modules/page_events';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login,
    events,
    page_events
  }
})