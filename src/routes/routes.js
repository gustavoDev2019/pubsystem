const Public = () => import("@/pages/Public.vue")
const Home = () => import("@/pages/Home/Home.vue")
const Drinks = () => import("@/pages/Drinks/Drinks.vue")
const EventList = () => import("@/pages/Events/EventList.vue")
const MenuList = () => import("@/pages/Menu/MenuList.vue")
const Menu = () => import("@/pages/Menu/Menu.vue")
const Press = () => import("@/pages/Press/Press.vue")
const Pub = () => import("@/pages/Pub/Pub.vue")
const Contact = () => import("@/pages/Contact/Contact.vue")

//Backoffice
const Login = () => import("@/pages/backoffice/Login.vue")
const Dashboard = () => import("@/pages/backoffice/dashboard/Dashboard.vue")
const Event = () => import("@/pages/backoffice/dashboard/events/Event.vue")
const ListEvents = () => import("@/pages/backoffice/dashboard/events/ListEvents.vue")
const EventTest = () => import("@/pages/Events/Event.vue")

const routes = [
  {
    path: '/login',
    component: Login
  },
  {
    path: '/dashboard',
    component: Dashboard,
    redirect: '/dashboard/event-list',
    /*meta: {
      requiresAuth: true
    },*/
    children: [
      {
        path: '/dashboard/event-register',
        component: Event
      },
      {
         path: '/dashboard/event-register/:url',
         component: Event
      },
      {
        path: '/dashboard/event-list',
        component: ListEvents
      },
    ]
  },
  { path: '/', redirect: '/home'},
  {
    path: '/',
    component: Public,
    redirect: '/home',
    children: [
      {
         path: "home",
         name: "home",
         component: Home
      },
      {
        path: '/show/:url',
        component: EventTest
      },
      {
        path: '/bebidas',
        component: Drinks
      },
      {
         path: '/agenda',
         component: EventList
      },
      {
         path: '/cardapios',
         component: MenuList
      },
      {
         path: '/cardapio',
         component: Menu
      },
      {
         path: '/imprensa',
         component: Press
      },
      {
         path: '/obar',
         component: Pub
      },
      {
         path: '/contato',
         component: Contact
      }
    ]
  },
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
