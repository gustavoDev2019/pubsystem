import Vue from "vue";
import App from "./App.vue";
import router from "./routes/index";
import store from './stores';
import { initialize } from './services/general.js';
import BootstrapVue from "bootstrap-vue";
import VueRouter from 'vue-router'
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

const VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);
Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.config.productionTip = false;

new Vue({
   router,
   store,
   render: h => h(App)
}).$mount("#app");
